
SIDs, = glob_wildcards("d0-AlignBam/{Sample}.dedup.bam")

## softwares
GATK4 = "/home/dell/biosoft/gatk-4.1.4.1"

## task
rule all:
    input:
        expand("e0-HaplotypeSplit/{Sample}_{I}.g.vcf.gz",I=range(1,32),Sample=SIDs)

rule Genotype_gatk:
    input:
        "c0-RefGenome/HaPG.fa",
        "d0-AlignBam/{Sample}.dedup.bam"
    output:
        expand("e0-HaplotypeSplit/{Sample}_{I}.g.vcf.gz",I=range(1,32),Sample=SIDs)
    shell:
        "{GATK4}/gatk HaplotypeCaller \
            --reference {input[0]} \
            --input {input[1]} \
            --output {output} \
            --emit-ref-confidence GVCF" \
            -L "chr"{wildcards.I}

##
rule SingleChr_gatk:
    input:
        "c0-RefGenome/HaPG.fa",
        expand("e0-HaplotypeSplit/{{Samples}}_{I}.g.vcf.gz",I=range(1,32))
    output:
        expand("f0-GenotypeSplit/{I}.vcf.gz",I=range(1,32))
    shell:
        "{GATK4}/gatk GenotypeGVCFs"
        "--reference {input[0]}"
        ""
