
SIDs, = glob_wildcards("b1-CleanData/{Sample}_paired_R1.fq.gz")

## softwares
GATK4 = "/home/dell/biosoft/gatk-4.1.4.1"

## task
rule all:
    input:
        expand("d0-AlignBam/{Sample}.dedup.bam",Sample=SIDs),
        expand("d0-AlignBam/{Sample}.dedup.matrics.txt",Sample=SIDs),
        expand("d0-AlignBam/{Sample}.dedup.bam.bai",Sample=SIDs)

rule Align_bwa:
    input:
        "c0-RefGenome/HaPG",
        "b1-CleanData/{Sample}_paired_R1.fq.gz",
        "b1-CleanData/{Sample}_paired_R2.fq.gz"
    output:
        temp("d0-AlignBam/{Sample}.sam")
    threads:47
    params:
        extra=r"-R '@RG\tID:IMT\tPL:ILLUMINA\tSM:{Sample}'"
    shell:
        "bwa mem -t {threads} {params.extra} {input} -o {output}"

rule Format_samtools:
    input:
        "d0-AlignBam/{Sample}.sam"
    output:
        temp("d0-AlignBam/{Sample}.sort.bam")
    threads:47
    shell:
        "samtools sort -@ {threads} -O bam -o {output} {input}"

rule Dedup_gatk:
    input:
        "d0-AlignBam/{Sample}.sort.bam"
    output:
        "d0-AlignBam/{Sample}.dedup.bam",
        #"d0-AlignBam/{Sample}.dedup.matrics.txt"
    threads:47
    shell:
        "sambamba markdup -t {threads} {input} {output}"
        #"{GATK4}/gatk MarkDuplicates --INPUT {input} --METRICS_FILE {output[1]} --OUTPUT {output[0]}"

rule Index_samtools:
    input:
        "d0-AlignBam/{Sample}.dedup.bam"
    output:
        "d0-AlignBam/{Sample}.dedup.bam.bai"
    threads:47
    shell:
        "samtools index {input} -@ {threads}"
