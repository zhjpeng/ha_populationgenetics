# PART 1: A0-MapReadsToRef.sh

#!/bin/bash

## define file routes
Main1=$(pwd)
Ref=${Main1}/a0-ref_genome
Clean=${Main1}/b0-clean-data
Bam=${Main1}/c0-align_bam

## define parametres
Thread=75
Name=HaPG

## define software routes
PIC=${GAT}/picardtools/picard-tools-1.119

## declare software version
bwa_v=0.7.17-r1188
picard_v=1.119
samtools_v=1.9
qualimap_v=2.2.1

## index genome
#bwa index -a bwtsw -p ${Ref}/${Name} ${Ref}/${Name}.fa

#samtools faidx ${Ref}/${Name}.fa

#java -jar ${PIC}/CreateSequenceDictionary.jar \
#          REFERENCE=${Ref}/${Name}.fa \
#          OUTPUT=${Ref}/${Name}.dict

## * Reads Alignment
#mkdir ${Bam}

for K in $(ls ${Clean} | grep ".fq.gz$" | awk -F '_' '{print $1}'| sort | uniq)
do
    bwa mem -t ${Thread} \
            -R "@RG\tID:foo\tSM:${K}\tLB:library1" \
            ${Ref}/${Name} \
            ${Clean}/${K}_paired_R1.fq.gz\
            ${Clean}/${K}_paired_R2.fq.gz |\
    samtools sort -@ ${Thread} -O bam -o ${Bam}/${K}.bam

    # remove multi-mapping reads
    samtools view -b -q 5 ${Bam}/${K}.bam > ${Bam}/${K}.filter.bam

    # mark duplicated reads
    java -jar ${PIC}/MarkDuplicates.jar \
            INPUT=${Bam}/${K}.filter.bam \
            OUTPUT=${Bam}/${K}_dedup.bam \
            METRICS_FILE=${Bam}/${K}_dedup.matrics.txt

    rm ${Bam}/${K}.bam ${Bam}/${K}.filter.bam

    samtools index ${Bam}/${K}_dedup.bam -@ ${Thread}
done

## * Evaluating NGS alignment data
### Creat configure file, col 1 is sample name and col 2 is path to BAM file (-r mode)
#mkdir -p ${Bam}/Zr_Qualimap
#ls ${Bam}| grep "dedup.bam$" | awk -v Var=${Bam} -F "_" '{print $1"\t"Var"/"$1"_dedup.bam"}' > ${Bam}/Zr_Qualimap/1-qualimap.config
#${QUA}/qualimap multi-bamqc -d ${Bam}/Zr_Qualimap/1-qualimap.config -outdir ${Bam}/Zr_Qualimap -outfile report.pdf -outformat PDF -r

#rm -r ${Bam}/*stats$


# PART2: B0-CallAndFilterVariants.sh
#!/bin/bash

## define file routes
Main1=$(pwd)
Ref=${Main1}/a0-ref_genome
Clean=${Main1}/b0-clean_data
Bam=${Main1}/c1-align_bam
Gvcf=${Main1}/d0-gvcf
Vcf=${Main1}/e0-vcf

## define parametres
Thread=48
Name=HaPG

## define software routes
GAT=/home/dell/biosoft
PIC=${GAT}/picardtools/picard-tools-1.119

## declare software version
GATK_v=3.8

## call variants by GATK

#mkdir ${Gvcf}
#for K in $(ls ${Bam} | grep "bam$" | awk -F '_' '{print $1}'|sort |uniq)
#do
#    java -jar ${PIC}/AddOrReplaceReadGroups.jar \
#       I=${Bam}/${K}_dedup.bam \
#       O=${Bam}/${K}_temp.bam \
#       RGID=foo \
#       RGLB=library1 \
#       RGPL=illumina \
#       RGPU=unit1 \
#       RGSM=${K}

#    rm ${Bam}/${K}_dedup.bam && mv ${Bam}/${K}_temp.bam  ${Bam}/${K}_dedup.bam
#    java -jar ${GAT}/GenomeAnalysisTK.jar \
#         -T HaplotypeCaller \
#         -R ${Ref}/${Name}.fa \
#         -I ${Bam}/${K}_dedup.bam \
#         --emitRefConfidence GVCF \
#         -o ${Gvcf}/${K}.g.vcf.gz \
#         -stand_call_conf 10 \
#         -A StrandAlleleCountsBySample \
#         -nct ${Thread}
#done

#--fix_misencoded_quality_scores \

#mkdir ${Vcf}
#for Q in Ac Aq ARG Aus Ay B C D F G Hami Hm I J K k M N P S5 Sc SEN Sw U X
#do
#    GVCFFILE=""
#    for K in $(ls ${Gvcf}| grep "g.vcf$" |grep ${Q} | awk -F '.' '{print $1}')
#    do
#        GVCFFILE=${GVCFFILE}"--variant ${Gvcf}/${K}.g.vcf "
#    done

#    java -jar ${GAT}/GenomeAnalysisTK.jar \
#         -T CombineGVCFs \
#         -R ${Ref}/${Name}.fa \
#         ${GVCFFILE} \
#         -o ${Vcf}/${Q}_cohort.g.vcf &
#    sleep 10m
#done

#wait

#GVCFFILE=""
#for K in $(ls ${Gvcf}| grep "g.vcf.gz$" | awk -F '.' '{print $1}')
#do
#    GVCFFILE=${GVCFFILE}"--variant ${Gvcf}/${K}.g.vcf.gz "
#done


#java -jar ${GAT}/GenomeAnalysisTK.jar \
#         -T CombineGVCFs \
#         -R ${Ref}/${Name}.fa \
#         ${GVCFFILE} \
#         -o ${Gvcf}/Cohort.g.vcf.gz

# genotype
#java -Xmx120g -jar ${GAT}/GenomeAnalysisTK.jar \
#     -T GenotypeGVCFs \
#     ${GVCFFILE} \
#     -R ${Ref}/${Name}.fa \
#     -o ${Vcf}/1-Basic.vcf \
#     -nt ${Thread}

# select SNPs
java -jar ${GAT}/GenomeAnalysisTK.jar \
    -T SelectVariants \
    -R ${Ref}/${Name}.fa \
    -V ${Vcf}/1-Basic.vcf \
    -o ${Vcf}/2-SelectSNP.vcf.gz \
    -selectType SNP \
    -nt ${Thread}

# hard filters of SNPs
java -jar ${GAT}/GenomeAnalysisTK.jar \
    -T VariantFiltration \
    -R ${Ref}/${Name}.fa \
    --variant ${Vcf}/2-SelectSNP.vcf.gz \
    -o ${Vcf}/3-HardFilter.vcf.gz \
    --filterName "S" \
    --filterExpression "QD < 2.0 || FS > 60.0 || MQ < 30.0 || HaplotypeScore > 13.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0" 2>${Vcf}/WarningSNPFilter.txt

# criterions of populations
vcftools --vcf ${Vcf}/3-HardFilter.vcf.gz \
           --remove-filtered-all \
           --max-alleles 2 \
           --max-alleles 2 \
           --minGQ 10 \
           --max-missing 0.7 \
           --maf 0.03 \
           --recode \
           --out ${Vcf}/4-HighQuality
