# Ha_PopulationGenetics

GOenrichment_clusterProfiler

1. 使用VCFtools滑窗计算区间内的Fst和Pi

2. 使用GenomeScan.R筛选符合要求的基因组区间

3. 使用SelectRegion2GeneName.py获取区间内的基因名称

4. 使用enrichment_GO.R进行富集分析

我们使用Admixtools来计算种群间是否存在基因流
