import sys

# python3 SelectRegion2GeneName.py ../../a0-ref_genome/Ha.trim.gff percent-3-region.txt | sort | uniq > percent-3-region.gene

for item1 in open(sys.argv[2]):
    item1 = item1.strip().split('\t')
    chrID = item1[0]
    posStart=int(item1[1])
    posEnd=int(item1[2])
    for line in open(sys.argv[1]):
        tab = line.strip().split("\t")
        if tab[2]=="mRNA":
            if tab[0]==chrID and int(tab[3])>=posStart and int(tab[3])<posEnd:
                print(tab[8].split("Name=")[1].split(";")[0])

            elif tab[0]==chrID and int(tab[4])>posStart and int(tab[4])<posEnd:
                print(tab[8].split("Name=")[1].split(";")[0])

            elif tab[0]==chrID and int(tab[3])<=posStart and int(tab[4])>posEnd:
                print(tab[8].split("Name=")[1].split(";")[0])
            else:
                next
