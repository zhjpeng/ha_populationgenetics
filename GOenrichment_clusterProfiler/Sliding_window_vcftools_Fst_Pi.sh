## 2020/06/07
#/home/dell/biosoft/plink1.9/plink --vcf /home/newsdd/e0-vcf/3-HardFilter.vcf --biallelic-only --vcf-min-gq 5 --geno 0.3 --maf 0.03 --remove-fam /home/newsdd/l0-selectivesweep/20200607/00sample_del.txt --allow-extra-chr --allow-no-sex --chr-set 32 --out /home/newsdd/l0-selectivesweep/20200607/a0-raw-vcf/4-HighQuality.vcf --threads 48 --recode vcf-iid

#cd a0-raw-vcf && gzip /home/newsdd/l0-selectivesweep/20200607/a0-raw-vcf/4-HighQuality.vcf.vcf && cd -

# calculate Fst
vcftools --gzvcf /home/newsdd/l0-selectivesweep/20200607/a0-raw-vcf/4-HighQuality.vcf.gz \
         --weir-fst-pop /home/newsdd/l0-selectivesweep/20200607/b0-group-information/Africa.list \
         --weir-fst-pop /home/newsdd/l0-selectivesweep/20200607/b0-group-information/NWC.list \
         --fst-window-size 10000 \
         --fst-window-step 5000 \
         --out c0-Africa-NWC/Africa-NWC
         
vcftools --gzvcf /home/newsdd/l0-selectivesweep/20200607/a0-raw-vcf/4-HighQuality.vcf.gz \
         --weir-fst-pop /home/newsdd/l0-selectivesweep/20200607/b0-group-information/NC.list \
         --weir-fst-pop /home/newsdd/l0-selectivesweep/20200607/b0-group-information/NWC.list \
         --fst-window-size 10000 \
         --fst-window-step 5000 \
         --out d0-NWC-NC/NWC-NC
         
vcftools --gzvcf /home/newsdd/l0-selectivesweep/20200607/a0-raw-vcf/4-HighQuality.vcf.gz \
         --weir-fst-pop /home/newsdd/l0-selectivesweep/20200607/b0-group-information/NC.list \
         --weir-fst-pop /home/newsdd/l0-selectivesweep/20200607/b0-group-information/Australia.list \
         --fst-window-size 10000 \
         --fst-window-step 5000 \
         --out e0-NC-Aus/NC-Aus
         
vcftools --gzvcf /home/newsdd/l0-selectivesweep/20200607/a0-raw-vcf/4-HighQuality.vcf.gz \
         --weir-fst-pop /home/newsdd/l0-selectivesweep/20200607/b0-group-information/NWC.list \
         --weir-fst-pop /home/newsdd/l0-selectivesweep/20200607/b0-group-information/Australia.list \
         --fst-window-size 10000 \
         --fst-window-step 5000 \
         --out f0-NWC-Aus/NWC-Aus

# pi && Tajima
for K in NC NWC Africa Australia
do
    vcftools --gzvcf /home/newsdd/l0-selectivesweep/20200607/a0-raw-vcf/4-HighQuality.vcf.gz \
             --keep /home/newsdd/l0-selectivesweep/20200607/b0-group-information/${K}.list \
             --window-pi 10000 \
             --window-pi-step 5000 \
             --out g0-pi/${K}
             
    vcftools --gzvcf /home/newsdd/l0-selectivesweep/20200607/a0-raw-vcf/4-HighQuality.vcf.gz \
             --keep /home/newsdd/l0-selectivesweep/20200607/b0-group-information/${K}.list \
             --min-alleles 2 \
             --max-alleles 2 \
             --TajimaD 5000 \
             --out h0-tajimaD/${K}
done
